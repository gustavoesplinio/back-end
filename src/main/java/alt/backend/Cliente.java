package alt.backend;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;

import javax.swing.text.MaskFormatter;

import com.fasterxml.jackson.annotation.JsonFormat;

import alt.vertx.ddd.PublicField;
import alt.vertx.memory.Identifiable;

public class Cliente  implements Identifiable {
    
    @PublicField
    public String id;
    @PublicField
    public Integer codigo;
    @PublicField
    public String nome;
    @PublicField
    public String endereco;
    @PublicField
    public BigDecimal telefone;

    @PublicField
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public Date dataNasc;

    
   

@Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public static String formatString(String value, String pattern) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);


        } catch (ParseException ex) {
            return value;
        }
        
    }

    public BigDecimal getTelefone() {
        return telefone;
    }

    public void setTelefone(BigDecimal telefone) {
        this.telefone = telefone;
    }

    
}
