package alt.backend;

import alt.vertx.memory.MemRepository;
import io.vertx.ext.web.Router;

public class RecursoCliente  extends MemRepository<Cliente>{

    public RecursoCliente(Router router) {
        super("/clientes", router);
    }

}
